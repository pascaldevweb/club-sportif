package com.pascal.clubsportif.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pascal.clubsportif.entities.OptionAbonnement;

public interface OptionAbonnementRepository extends JpaRepository<OptionAbonnement, Long> {

    OptionAbonnement findByNomOptionAbonnement(String nomOptionAbonnement);
    
}
