package com.pascal.clubsportif.entities;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ContratBeneficiareNonPayeur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idContratBeneficiaireNonPayeur;
    private boolean verifContratBeneficiaireNonPayeur;
    private String nomContratBeneficiaireNonPayeur;
    private String prenomContratBeneficiaireNonPayeur;
    private String emlailContratBeneficiaireNonPayeur;
    private Date dateNaissanceContratBeneficiaireNonPayeur;
    private Long TelContratBeneficiaireNonPayeur;
    private String etablissementBanqueContratBeneficiaireNonPayeur;
    private String IBANContratBeneficiaireNonPayeur;
    private String BICContratBeneficiaireNonPayeur;
    
    // @JsonIgnore
    // @OneToMany(mappedBy = "contratBeneficiareNonPayeur")
    // private List<Users> users;
}
