package com.pascal.clubsportif.entities;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.persistence.JoinColumn;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Users{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUser;
    @Column(unique=true)
    private String emailUser;
    private String passwordUser;
    private String nomUser;
    private String prenomUser;
    private Date dateNaissanceUser;
    private String adresseUser;
    private Long telephoneUser;
    private Long CpUser;
    private String villeUser;
    private String paysUser;
    
    private boolean progEntrainementUser;
    private boolean emailingUser;
    private boolean cgVenteUser;

    
    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="user_role",joinColumns = @JoinColumn(name="user_id") ,
    inverseJoinColumns = @JoinColumn(name="role_id"))
    private List<Roles> roles;

    @ManyToOne
    private Club club;
    @ManyToOne
    private Abonnement abonnement;
    
    @ManyToOne
    private Civilite civilite;
    @ManyToOne
    private ContratBeneficiairePayeur contratBeneficiairePayeur;
    // @ManyToOne
    // private ContratBeneficiareNonPayeur contratBeneficiareNonPayeur;
    public void getRoles(Roles role) {
    }
  



}
