package com.pascal.clubsportif.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pascal.clubsportif.entities.Paiement;

public interface PaiementRepository extends JpaRepository <Paiement,Long> {
    
    Paiement findByNomProprietairePaiement(String nomProprietairePaiement);
}
