package com.pascal.clubsportif.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pascal.clubsportif.entities.Civilite;
import com.pascal.clubsportif.repos.CiviliteRepository;

@RestController
@RequestMapping("/api/civilite")
@CrossOrigin("*")
public class CiviliteRestController {
    @Autowired
    CiviliteRepository civiliteRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Civilite> getAllcivilites(){
        return civiliteRepository.findAll();
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public Civilite getCiviliteById(@PathVariable("id") Long id) {
        return civiliteRepository.findById(id).get();
    }
}
