package com.pascal.clubsportif.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pascal.clubsportif.entities.Abonnement;
import com.pascal.clubsportif.entities.Civilite;
import com.pascal.clubsportif.entities.Club;
import com.pascal.clubsportif.entities.Roles;
import com.pascal.clubsportif.entities.Users;
import com.pascal.clubsportif.repos.UsersRepository;
import com.pascal.clubsportif.repos.AbonnementRepository;
import com.pascal.clubsportif.repos.CiviliteRepository;
import com.pascal.clubsportif.repos.ClubRepository;
import com.pascal.clubsportif.repos.RolesRepository;

@Service
@Transactional
public class UsersServiceImpl implements UsersService{

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    CiviliteRepository civiliteRepository;

    @Autowired
    RolesRepository rolesRepository;

    @Autowired
    AbonnementRepository abonnementRepository;

    @Autowired
    ClubRepository clubRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Users saveUsers(Users users) {
        users.setPasswordUser(bCryptPasswordEncoder.encode(users.getPasswordUser()));
        return usersRepository.save(users);
    }

    @Override
    public Users findByNomService(String nomUser) {
        
        return usersRepository.findByNomUser(nomUser);
    }

    @Override
    public Civilite addCivilite(Civilite civilite) {
        return civiliteRepository.save(civilite);
    }

    @Override
    public Users addCiviliteToUser(String nomUser, String civiliteNom) {
        Users usr=usersRepository.findByNomUser(nomUser);
        Civilite cvlt=civiliteRepository.findByNomCivilite(civiliteNom);
        
        usr.setCivilite(cvlt);
        
        return usr;
    }

    @Override
    public Roles addRoles(Roles roles) {
      
        return rolesRepository.save(roles);
    }

    @Override
    public Users addRolesToUser(String nomUser, String roleNom) {
     Users usr=usersRepository.findByNomUser(nomUser);
     Roles rol=rolesRepository.findByNomRoles(roleNom);
     usr.setRoles(rol);
        return usr;
    }

   

    @Override
    public Users addAbonnementToUser(String nomUser, String abonnementNom) {
        Users usr = usersRepository.findByNomUser(nomUser);
        Abonnement abnmt= abonnementRepository.findByNomAbonnement(abonnementNom);
        usr.setAbonnement(abnmt);
        return usr;
    }

    @Override
    public Club addClub(Club club) {
        return clubRepository.save(club);
    }

    @Override
    public Users addClubToUser(String nomUser, String clubNom) {
        Users usr=usersRepository.findByNomUser(nomUser);
        Club club=clubRepository.findByVilleClub(clubNom);
        usr.setClub(club);
        return usr;
    }

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    } 

    }

    
    

