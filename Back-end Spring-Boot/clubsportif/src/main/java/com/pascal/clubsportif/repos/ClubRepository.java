package com.pascal.clubsportif.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.pascal.clubsportif.entities.Club;


@RepositoryRestResource(path="club")
@CrossOrigin("http://localhost:4200/")
public interface ClubRepository extends JpaRepository <Club, Long>{
    
   Club findByVilleClub(String villeClub); 
}
