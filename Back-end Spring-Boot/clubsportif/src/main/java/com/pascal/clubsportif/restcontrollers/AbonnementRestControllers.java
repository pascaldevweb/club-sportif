package com.pascal.clubsportif.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pascal.clubsportif.entities.Abonnement;

import com.pascal.clubsportif.repos.AbonnementRepository;


@RestController
@RequestMapping("/api/abonnement")
@CrossOrigin("*")
public class AbonnementRestControllers {
    @Autowired
    AbonnementRepository abonnementRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Abonnement> getAllabonnements(){
        return abonnementRepository.findAll();
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public Abonnement getClubById(@PathVariable("id") Long id) {
        return abonnementRepository.findById(id).get();
    }
}