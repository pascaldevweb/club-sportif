package com.pascal.clubsportif.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pascal.clubsportif.entities.ContratBeneficiairePayeur;

public interface ContratBeneficiarePayeurRepository extends JpaRepository <ContratBeneficiairePayeur, Long> {

    ContratBeneficiairePayeur findByIBANContratBeneficairePayeur(String iBANContratBeneficairePayeur);
    
}
