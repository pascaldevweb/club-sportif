package com.pascal.clubsportif.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pascal.clubsportif.entities.Roles;

public interface RolesRepository extends JpaRepository<Roles, Long>{
    

    Roles findByNomRoles(String nomRoles);
}
