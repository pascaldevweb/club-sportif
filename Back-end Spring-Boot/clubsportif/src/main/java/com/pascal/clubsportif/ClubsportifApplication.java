package com.pascal.clubsportif;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.pascal.clubsportif.entities.Abonnement;
import com.pascal.clubsportif.entities.Civilite;
import com.pascal.clubsportif.entities.Club;
import com.pascal.clubsportif.entities.OptionAbonnement;
import com.pascal.clubsportif.entities.Roles;
import com.pascal.clubsportif.entities.Users;
import com.pascal.clubsportif.service.AbonnementService;
import com.pascal.clubsportif.service.UsersService;

import jakarta.annotation.PostConstruct;

@SpringBootApplication
public class ClubsportifApplication implements CommandLineRunner{


	@Autowired
	private RepositoryRestConfiguration repositoryRestConfiguration;
	public static void main(String[] args) {
		SpringApplication.run(ClubsportifApplication.class, args);
	}


	@Autowired
	UsersService usersService;

	@Autowired
	AbonnementService abonnementService;



	@PostConstruct
	void init_users(){
		
		//usersService.saveUsers(new Users(null, "test@gmail.com", "azerty", null, null, null, null, null, null, null, null, false, false, false, null, null, null, null, null));

		// usersService.addCivilite(new Civilite(null,"Mr"));
		// usersService.addCivilite(new Civilite(null,"Mme"));
		// usersService.addCivilite(new Civilite(null,"Mlle"));

		// usersService.addRoles(new Roles(null, "Visiteur"));
		// usersService.addRoles(new Roles(null, "Adhérent"));
		// usersService.addRoles(new Roles(null, "Coach"));
		// usersService.addRoles(new Roles(null, "Administrateur"));

		// usersService.addClub(new Club(null,"1 rue de la république",75000L,"Paris"));
		// usersService.addClub(new Club(null,"10 rue de Bretagne",35000L,"Rennes"));
		// usersService.addClub(new Club(null,"1 rue du Sud",13000L,"Marseille"));
		// usersService.addClub(new Club(null,"1 rue du Tribunal",35130L,"Fougères"));
		// usersService.addClub(new Club(null,"1 rue du sportif",69000L,"Lyon"));


		// abonnementService.addAbonnement(new Abonnement(null,"Intensif","Entrées illimité",30,null));
		// abonnementService.addAbonnement(new Abonnement(null,"Sportif","5 entrées par semaines",20,null));
		// abonnementService.addAbonnement(new Abonnement(null,"Découverte","2 entrées par semaines",10,null));

		// abonnementService.addOptionAbonnement(new OptionAbonnement(null,"Douche","Accès au douche du club",5.0));
		// abonnementService.addOptionAbonnement(new OptionAbonnement(null,"Boisson","Accès machine boisson protéinées",5.0));
		// abonnementService.addOptionAbonnement(new OptionAbonnement(null,"All clubs","Accès à tous les club de france",5.0));
	}

	@Override
		public void run(String... args) throws Exception {
		repositoryRestConfiguration.exposeIdsFor(Civilite.class,Users.class, Club.class, Abonnement.class);
}

@Bean
BCryptPasswordEncoder getBCE() {
return new BCryptPasswordEncoder();
 }
	

}
