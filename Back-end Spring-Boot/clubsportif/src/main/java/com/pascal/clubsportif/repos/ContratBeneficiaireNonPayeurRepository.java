package com.pascal.clubsportif.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pascal.clubsportif.entities.ContratBeneficiareNonPayeur;

public interface ContratBeneficiaireNonPayeurRepository extends JpaRepository<ContratBeneficiareNonPayeur,Long> {

    ContratBeneficiareNonPayeur findByNomContratBeneficiaireNonPayeur(String nomContratBeneficiaireNonPayeur);
    
}
