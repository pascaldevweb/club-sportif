package com.pascal.clubsportif.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Civilite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCivilite;
    private String nomCivilite;

    // @JsonIgnore
    // @OneToMany(mappedBy = "civilite")
    // private List<Users> users;
}
