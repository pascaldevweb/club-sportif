package com.pascal.clubsportif.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.pascal.clubsportif.entities.Users;

@RepositoryRestResource(path="rest")

public interface UsersRepository extends JpaRepository<Users, Long>{

    Users  findByNomUser(String nomUser);
    
}
