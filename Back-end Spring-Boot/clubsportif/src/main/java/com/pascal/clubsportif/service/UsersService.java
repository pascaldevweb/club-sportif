package com.pascal.clubsportif.service;

import java.util.List;


import com.pascal.clubsportif.entities.Civilite;
import com.pascal.clubsportif.entities.Club;
import com.pascal.clubsportif.entities.Users;
import com.pascal.clubsportif.entities.Roles;

public interface UsersService {
    Users saveUsers(Users users);
    Users findByNomService (String nomUser);
    Civilite addCivilite(Civilite civilite);
    Users addCiviliteToUser(String nomUser, String civiliteNom);
    Roles addRoles(Roles roles);
    Users addRolesToUser(String nomUser, String roleNom);
    Users addAbonnementToUser(String nomUser, String abonnementNom);
    Club addClub(Club club);
    Users addClubToUser(String nomUser, String clubNom);
    List<Users> getAllUsers();
    
}
    

