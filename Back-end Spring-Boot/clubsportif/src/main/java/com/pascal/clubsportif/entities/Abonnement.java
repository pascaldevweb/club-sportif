package com.pascal.clubsportif.entities;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Abonnement {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long idAbonnement;
    private String nomAbonnement;
    private String descriptionAbonnement;
    private double prixMensuelAbonnement;
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name="abonnement_option",joinColumns = @JoinColumn(name="abonnement_id"),
    inverseJoinColumns=@JoinColumn(name="OptionAbonnement_id"))
    private List<OptionAbonnement>listOptions;

    // @JsonIgnore
    // @OneToMany(mappedBy = "abonnement")
    // private List<Users> users;
    
}
