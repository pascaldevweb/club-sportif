package com.pascal.clubsportif.service;

import com.pascal.clubsportif.entities.Abonnement;
import com.pascal.clubsportif.entities.OptionAbonnement;

public interface AbonnementService {
    Abonnement addAbonnement(Abonnement abonnement);
    OptionAbonnement addOptionAbonnement(OptionAbonnement optionAbonnement);
    Abonnement AddOptionAbonnementToAbonnement(String nomAbonnement, String nomOptionAbonnement);
}
