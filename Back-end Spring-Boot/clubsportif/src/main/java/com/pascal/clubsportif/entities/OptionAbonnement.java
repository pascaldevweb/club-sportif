package com.pascal.clubsportif.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OptionAbonnement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOptionAbonnement;
    private String nomOptionAbonnement;
    private String descriptionOptionAbonnement;
    private Double prixOptionAbonnement;
    
}
