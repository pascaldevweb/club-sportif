package com.pascal.clubsportif.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.pascal.clubsportif.entities.Users;
import com.pascal.clubsportif.service.UsersService;

public class MyUserDetailService implements UserDetailsService{
    @Autowired
    UsersService usersService;
    @Override
    public UserDetails loadUserByUsername(String nomUser) throws UsernameNotFoundException {
        
        Users user = usersService.findByNomService(nomUser);
        if (user==null)
        throw new UsernameNotFoundException("Utilisateur introuvable !");
        List<GrantedAuthority> auths = new ArrayList<>();
        user.getRoles().forEach(role -> {
        GrantedAuthority auhority = new
        SimpleGrantedAuthority(role.getNomRoles());
        auths.add(auhority);
        });
        return new org.springframework.security.core.
        
        userdetails.User(user.getEmailUser(),user.getPasswordUser(),auths);
        
        }
        
    }
    
