package com.pascal.clubsportif.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pascal.clubsportif.entities.Users;
import com.pascal.clubsportif.service.UsersService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class UsersRestControllers {
    
    @Autowired
    UsersService usersService;


    @RequestMapping(method = RequestMethod.GET)
    public List<Users>getAllUsers() {
        return usersService.getAllUsers();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Users createUser(@RequestBody Users user){
        return usersService.saveUsers(user);
    }
}
