package com.pascal.clubsportif.repos;
import com.pascal.clubsportif.entities.Civilite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;


@RepositoryRestResource(path="civilite")
@CrossOrigin("http://localhost:4200/")
public interface CiviliteRepository extends JpaRepository<Civilite, Long>{

    Civilite findByNomCivilite(String nomCivilite);
    
}
