package com.pascal.clubsportif.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import com.pascal.clubsportif.entities.Abonnement;
import com.pascal.clubsportif.entities.OptionAbonnement;
import com.pascal.clubsportif.repos.AbonnementRepository;
import com.pascal.clubsportif.repos.OptionAbonnementRepository;

@Service
@Transactional
public class AbonnementServiceImpl implements AbonnementService{
    @Autowired
    AbonnementRepository abonnementRepository;

    @Autowired
    OptionAbonnementRepository optionAbonnementRepository;

    @Override
    public Abonnement addAbonnement(Abonnement abonnement) {
        return abonnementRepository.save(abonnement);
    }


    @Override
    public OptionAbonnement addOptionAbonnement(OptionAbonnement optionAbonnement) {
        return optionAbonnementRepository.save(optionAbonnement);
    }


    @Override
    public Abonnement AddOptionAbonnementToAbonnement(String nomAbonnement, String nomOptionAbonnement) {
        Abonnement abnmt=abonnementRepository.findByNomAbonnement(nomAbonnement);
        OptionAbonnement option=optionAbonnementRepository.findByNomOptionAbonnement(nomOptionAbonnement);
        abnmt.getListOptions().add(option);
        return abnmt;
    }
}
