package com.pascal.clubsportif.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ContratBeneficiairePayeur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idContratBeneficairePayeur;
    private Boolean RIBContratBeneficairePayeur;
    private Boolean EtatContratBeneficairePayeur;
    private Boolean beneficiaireNonPayeurContratBeneficairePayeur;
    private String etablissementContratBeneficairePayeur;
    private String IBANContratBeneficairePayeur;
    private String BICContratBeneficairePayeur;
    
    @JsonIgnore
    @OneToMany(mappedBy = "contratBeneficiairePayeur")
    private List<Users> users;
}
