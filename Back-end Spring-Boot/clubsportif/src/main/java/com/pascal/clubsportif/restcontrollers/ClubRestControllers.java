package com.pascal.clubsportif.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.pascal.clubsportif.entities.Club;

import com.pascal.clubsportif.repos.ClubRepository;

@RestController
@RequestMapping("/api/club")
@CrossOrigin("*")
public class ClubRestControllers {
    @Autowired
    ClubRepository clubRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Club> getAllClubs(){
        return clubRepository.findAll();
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public Club getClubById(@PathVariable("id") Long id) {
        return clubRepository.findById(id).get();
    }
}