import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CiviliteComponent } from './civilite/civilite.component';
import { AbonnementComponent } from './abonnement/abonnement.component';
import { ClubComponent } from './club/club.component';
import { ContratBeneficiairePayeurComponent } from './contrat-beneficiaire-payeur/contrat-beneficiaire-payeur.component';
import { ContratBeneficiaireNonPayeurComponent } from './contrat-beneficiaire-non-payeur/contrat-beneficiaire-non-payeur.component';
import { OptionAbonnementComponent } from './option-abonnement/option-abonnement.component';
import { PaiementComponent } from './paiement/paiement.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
  {path:"login", component:LoginComponent},
  {path:"register",component:RegisterComponent},
  {path:"users",component:UsersComponent},
  {path:"abonnement",component:AbonnementComponent},
  {path:"civilite",component:CiviliteComponent},
  {path:"club",component:ClubComponent},
  {path:"contratBeneficiairePayeur",component:ContratBeneficiairePayeurComponent},
  {path:"contratBeneficiaireNonPayeur",component:ContratBeneficiaireNonPayeurComponent},
  {path:"optionAbonnement",component:OptionAbonnementComponent},
  {path:"paiement",component:PaiementComponent},
  {path:"roles",component:RolesComponent},
  {path:"home",component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
