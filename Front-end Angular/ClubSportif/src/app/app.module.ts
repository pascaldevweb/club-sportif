import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { AbonnementComponent } from './abonnement/abonnement.component';
import { CiviliteComponent } from './civilite/civilite.component';
import { ClubComponent } from './club/club.component';
import { ContratBeneficiairePayeurComponent } from './contrat-beneficiaire-payeur/contrat-beneficiaire-payeur.component';
import { ContratBeneficiaireNonPayeurComponent } from './contrat-beneficiaire-non-payeur/contrat-beneficiaire-non-payeur.component';
import { OptionAbonnementComponent } from './option-abonnement/option-abonnement.component';
import { PaiementComponent } from './paiement/paiement.component';
import { RolesComponent } from './roles/roles.component';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UsersComponent,
    AbonnementComponent,
    CiviliteComponent,
    ClubComponent,
    ContratBeneficiairePayeurComponent,
    ContratBeneficiaireNonPayeurComponent,
    OptionAbonnementComponent,
    PaiementComponent,
    RolesComponent,
    HomeComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
