import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Users } from './model/users.model';

const httpOptions={
  headers:new HttpHeaders({'Content-Type':'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  apiURL: string ='http://localhost:8080/clubsportif/api';
  constructor(private http : HttpClient) { 

  }

  ajouterUser(users : Users):Observable<Users>{
    return this.http.post<Users>(this.apiURL,users,httpOptions);
  }

}

