import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Abonnement } from './model/abonnement.model';
import { AbonnementWrapped } from './model/abonnementWrapped.model';
import { Civilite } from './model/civilite.model';
import { CiviliteWrapped } from './model/CiviliteWrapped.model';
import { Club } from './model/club.model';
import { ClubWrapped } from './model/clubWrapped.model';
import { Users } from './model/users.model';


const httpOptions={
  headers:new HttpHeaders({'Content-Type':'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiURLCivilite:string='http://localhost:8080/clubsportif/civilite';
  apiURLClub:string='http://localhost:8080/clubsportif/club';
  apiURLAbonnement:string='http://localhost:8080/clubsportif/abonnement';
  users! : Users[];
  civilites! : Civilite[];
  clubs!: Club[];
  abonnements!: Abonnement[];



  constructor(private http:HttpClient) { }

  listeCivilite():Observable<CiviliteWrapped>{
    return this.http.get<CiviliteWrapped>(this.apiURLCivilite);
  }

  consulterCivilite(id:number): Civilite{
    return this.civilites.find(civilite  => civilite.idCivilite == id)!;
  }

  listeClub():Observable<ClubWrapped>{
    return this.http.get<ClubWrapped>(this.apiURLClub)
  }

  listeAbonnement():Observable<AbonnementWrapped>{
    return this.http.get<AbonnementWrapped>(this.apiURLAbonnement)
  }


}
