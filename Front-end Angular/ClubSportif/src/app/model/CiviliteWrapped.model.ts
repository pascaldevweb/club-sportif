import { Civilite } from "./civilite.model";

export class CiviliteWrapped{
    _embedded!: {civilites:Civilite[]};
}