import { OptionAbonnement } from "./optionAbonnement.model";

export class Abonnement{

    idAbonnement! : number;
    nomAbonnement! : string;
    descriptionAbonnement! : string;
    prixMensuelAbonnement!: number;
    listOption! : OptionAbonnement;

}