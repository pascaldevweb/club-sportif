export class OptionAbonnement{
    idOptionAbonnement! : number;
    nomOptionAbonnement! : string;
    descriptionOptionAbonnement! : string;
    prixOptionAbonnement! : number;
}