export class ContratBeneficiaireNonPayeur{
    idContratBeneficiaireNonPayeur! : number;
    verifContratBeneficiaireNonPayeur! : boolean;
    nomContratBeneficiaireNonPayeur! : string;
    prenomContratBeneficiaireNonPayeur! : string;
    emlailContratBeneficiaireNonPayeur! : string;
    dateNaissanceContratBeneficiaireNonPayeur! : Date;
    TelContratBeneficiaireNonPayeur! : number;
    etablissementBanqueContratBeneficiaireNonPayeur! : string;
    IBANContratBeneficiaireNonPayeur! : string;
    BICContratBeneficiaireNonPayeur! : string;
}