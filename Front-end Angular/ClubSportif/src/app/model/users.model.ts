import { Abonnement } from "./abonnement.model";
import { Civilite } from "./civilite.model";
import { Club } from "./club.model";
import { ContratBeneficiaireNonPayeur } from "./contratBeneficiaireNonPayeur.model";
import { ContratBeneficiairePayeur } from "./contratBeneficiairePayeur.model";
import { Roles } from "./roles.model";

export class Users{
    idUser! : number;
    nomUser! : string;
    prenomUser! : string;
    emailUser! : string;
    dateNaissanceUser! : Date;
    adresseUser! : string;
    telephoneUser! : number;
    CpUser! : number;
    villeUser! : string;
    paysUser! : string;
    passwordUser! : string;
    progEntrainementUser! : boolean;
    emailingUser! : boolean;
    cgVenteUser! : boolean;

    roles! : Roles ;
    club! : Club ;
    abonnement! : Abonnement ;
    civilite! : Civilite ;
    contratBeneficiairePayeur! : ContratBeneficiairePayeur ;
    contratBeneficiareNonPayeur! : ContratBeneficiaireNonPayeur ;


}