export class ContratBeneficiairePayeur{
    idContratBeneficairePayeur! : number;
    RIBContratBeneficairePayeur! : boolean;
    EtatContratBeneficairePayeur! : boolean;
    beneficiaireNonPayeurContratBeneficairePayeur! :boolean ;
    etablissementContratBeneficairePayeur! : string;
    IBANContratBeneficairePayeur! : string;
    BICContratBeneficairePayeur! : string;
}