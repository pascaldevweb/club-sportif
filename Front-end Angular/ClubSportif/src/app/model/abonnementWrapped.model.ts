import { Abonnement } from "./abonnement.model";

export class AbonnementWrapped{
    _embedded!: {abonnements:Abonnement[]};
}