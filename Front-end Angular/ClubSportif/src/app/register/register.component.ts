import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Abonnement } from '../model/abonnement.model';
import { Civilite } from '../model/civilite.model';
import { Club } from '../model/club.model';
import { Users } from '../model/users.model';
import { RegisterService } from '../register.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent implements OnInit{
  
  newUser= new Users();
  civilites!: Civilite[];
  abonnements!:Abonnement[];
  clubs!:Club[];

  newIdCivilite! : number;
  newCivilite!: Civilite;

  newIdClub! : number;
  newClub!: Club;

  newIdAbonnement! : number;
  newAbonnement!: Abonnement;


  constructor(private registerService :RegisterService,private userService: UserService, private router : Router){
    
  }
  
  ngOnInit(): void {
    this.userService.listeCivilite().subscribe(cvlts=>{
      console.log(cvlts);
      this.civilites=cvlts._embedded.civilites;
    })

    this.userService.listeAbonnement().subscribe(abnmt=>{
      this.abonnements=abnmt._embedded.abonnements;
    })

    this.userService.listeClub().subscribe(club=>{
      this.clubs=club._embedded.clubs;
    })
  }

  addUser(){
    
    this.newUser.civilite=this.civilites.find(cvlt=> cvlt.idCivilite==this.newIdCivilite)!;
    this.newUser.club=this.clubs.find(clb=> clb.idClub==this.newIdClub)!;
    this.newUser.abonnement=this.abonnements.find(Abonmt=> Abonmt.idAbonnement==this.newIdAbonnement)!;
    this.registerService.ajouterUser(this.newUser).subscribe(user =>{
      this.router.navigate(['login'])});
  }



  
}
